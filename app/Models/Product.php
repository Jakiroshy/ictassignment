<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $guarded = [];
    public function ProductAttributes(){
       return $this->hasMany(ProductAttribute::class);
    }

    public function SaveProductAttributes($data){
        foreach (json_decode($data,true) as $key => $value) {
            ProductAttribute::create([
                "key"=>$key,
                "value"=>$value,
                "product_id"=>$this->id
            ]);
        }
    }
}
