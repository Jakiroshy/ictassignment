<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use Illuminate\Auth\Events\Login;

class AuthController extends Controller
{
      /*
    * Handle user registering
    */
    public function register(StoreUserRequest $request)
    {
        $user = User::create(
            [
                "name" => $request->name,
                "password" => bcrypt($request->password),
                "email" => $request->email
            ]
        );
        return response()->json(
            [
                "msg" => "Account created. Login to access products."
            ],
            200
        );
    }
    /*
    * Handle user loging in
    */
    public function login(LoginRequest $request)
    {
        $data = [
            "email" => $request->email,
            "password" => $request->password
        ];
        if (auth()->attempt($data)) {

            return response()->json(
                [
                    "access_token" => auth()->user()->createToken("arandomstring")->plainTextToken
                ],
                200
            );
        } else {
            return response()->json(
                [
                    "error_msg" => "Not correct login details"
                ],
                401
            );
        }
    }
}
